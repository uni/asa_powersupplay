# ASA PowerSupplay Project Q3 2020

## Initial Meeting 15/02/21

The goal is to reproduce the results of the [paper by Guri](https://arxiv.org/pdf/2005.00395.pdf). This involves the following steps:

- [x] Write the program which affects PSU frequency patterns in Python

- [ ] Implement a transmission protocol for data exfiltration (in the paper, OFDM is used)

- [ ] Write a mobile app for demodulation. If too cumbersome, write a python script which can demodulate mic data.

- [ ] Dive into countermeasures, and see how they affect results.

- [ ] Compare communication protocols for efficiency / error rate.

## ToDos for Monday 22/02/21:

- [x] Formalize requirements

- [x] Initial PSU manipulation PoC Script

## ToDos for Monday 01/03/21:

- [x] Measure PSU signal using microphone - analyze spectrogram (look for available apps on Play Store)

- [x] Using the measurement above, find maximum rate at which we can switch

- [x] Explore differences between (B-)FSK and OFDM modulation (**we have to see later whether our signal is suitable for OFDM**), first transmission protocol draft (**see below**)

Leo: watch for optimization during program compilation


## ToDos for Tuesday 09/03/21 10:00:

- [x] Reproduce sweep signal (Fig. 16 & 17 in the paper) - failed

- [x] Reproduce Fig. 10 (BFSK signal)


## ToDos for Tuesday 16/03/21 10:00

- [x] Find solution for low SNR 

- [x] Unify past results

- [x] Test sweep signal / BFSK on different targets, with different parameters

- [ ] If there's time, get started on demodulation concepts


## ToDos for Tuesday 23/03/21 10:00

- [ ] Low SNR: Filter out lower frequency bands with high pass filter / band pass filter

- [ ] Define Frequency Bands for our respective targets (see Table III in the paper)

